FROM node:lts-alpine

RUN npm install -g wrangler

ENV USER=node
USER node

CMD ["wrangler"]
